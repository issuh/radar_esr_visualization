#include <iostream>
#include <memory>
#include <bitset>
#include <cmath>
#include <map>
#include <mutex>
#include <cstdint>
#include <arpa/inet.h> 

// ROS
#include <ros/ros.h>
#include <can_msgs/Frame.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

template <class T>
T reverse(T from)
{
  T to;

  char *s_from = reinterpret_cast<char*>(&from);
  char *s_to   = reinterpret_cast<char*>(&to);

  for (int i = 0; i < sizeof(T); ++i)
    s_to[i] = s_from[ sizeof(T) - i - 1 ];

  return to;
}

class Radar{
public:
    Radar(){
        n_ = std::unique_ptr<ros::NodeHandle>(new ros::NodeHandle("~"));
        
        sub_ = n_->subscribe("/can/esr_can/can_raw", 10, &Radar::callback, this);
        pub_ = n_->advertise<visualization_msgs::MarkerArray>("/radar", 100);
        // pub_ = n_->advertise<visualization_msgs::Marker>("/radar", 100);

      
        marker_.header.frame_id = "velodyne";  
        marker_.type = visualization_msgs::Marker::CUBE;
        marker_.action = visualization_msgs::Marker::ADD;
        marker_.lifetime = ros::Duration();

        marker_.color.a = 1.0;
        marker_.color.r = 0.0f;
        marker_.color.g = 1.0f;
        marker_.color.b = 0.0f;
        
        marker_.scale.x = 0.3;
        marker_.scale.y = 0.3;
        marker_.scale.z = 0.3;

    }

	void callback(const can_msgs::Frame &msg){
        char test[8] = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
        float angle = 0.0, range = 0.0;
        short buffer;
        char status;        
        if(msg.id >= 0x500 && msg.id <= 0x53f){    
            status = (msg.data[1] >> 5) & 0x07;
            if( 1 <= status && status <=5 ){
            // if( status == 3 ){

                // std::bitset<8> d0(msg.data[0]), d1(msg.data[1]),d2(msg.data[2]),d3(msg.data[3]),d4(msg.data[4]),d5(msg.data[5]),d6(msg.data[6]),d7(msg.data[7]);
                // printf("----------------------------------\n[%x] %x %x %x %x %x %x %x %x\n", msg.id ,msg.data[0], msg.data[1], msg.data[2], msg.data[3],msg.data[4],msg.data[5],msg.data[6],msg.data[7]);

                char isNegative = (msg.data[1] >> 4) & 0x01;
                if(isNegative){
                    buffer = (((short)msg.data[1] & 0x001F) << 5 | ((short)msg.data[2] & 0x00f8) >> 3);
                    buffer = buffer + 0xfc00;
                    angle = (float)buffer * 0.1;           
                }
                else
                    angle = (( (msg.data[1] << 5) | (msg.data[2] >> 3) ) & 0x03ff) * 0.1; 



                range = (((msg.data[2] << 8) | msg.data[3]) & 0x07ff) * 0.1;

                if(range > 204.7 || range < 0){
                    printf("********************RANGE FAIL*********************\n");  
                    printf("range : %f\n", range);
                }  
               
                if( angle < -51.2 || angle > 51.1){
                    printf("********************ANGLE FAIL*********************\n");
                    printf("angle : %f\n\n", angle);              
                }

                // printf("range : %f\n", range);
                // printf("angle : %f\n\n", angle);              
                // printf("status : %d\n\n", status);              
                
                angle = 3.14159 * angle / 180;
                marker_.id = msg.id;
                marker_.text = std::to_string(msg.id);
                marker_.pose.position.x = range * cos(angle);
                marker_.pose.position.y = range * sin(angle);
                marker_.pose.position.z = 0;

                markerArray_.markers.push_back(marker_);

                if(markerArray_.markers.size() > 64){
                    pub_.publish(markerArray_);
                    markerArray_.markers.clear();
                }

                // pub_.publish(marker_);

            }
        }
	}

    void run(){
        while(1){
            
            mtx.lock();

            for(auto i : map)
                markerArray_.markers.push_back(i.second);
            
            pub_.publish(markerArray_);

            map.clear();
        
            mtx.unlock();

            ros::spinOnce();

            ros::Rate(1);
        }
    }

public:
	std::unique_ptr<ros::NodeHandle> n_;
	ros::Publisher pub_;
	ros::Subscriber sub_;

    std::map<int, visualization_msgs::Marker> map;

    visualization_msgs::Marker marker_;
    visualization_msgs::MarkerArray markerArray_;

    std::string subTopic;
    std::string pubTopic;

    std::mutex mtx;
};


int main(int argc, char **argv){
	ros::init(argc, argv, "Radar");	

	Radar sample;

	// sample.run();
    
    ros::spin();

}